package homework6
fun main(){
    val alex = Lion("Alex", 90, 190)
    val vitaly = Tiger("Vitaly", 80, 310)
    val gloria = Hippo("Gloria", 130, 1300)
    val kowalski = Wolf("Kowalski", 80, 80)
    val melman = Giraffe("Melman", 420, 1200)
    val sonya = Elephant("Sonya", 280, 3000)
    val mason = Chimpanzee("Mason", 120, 50)
    val julien = Gorilla("Julien", 160, 180)
    val listOfAnimals = arrayOf(alex, vitaly, gloria, kowalski, melman, sonya, mason, julien)
    val listOfFood = arrayOf("Meat", "Milk", "Fish", "Bones", "Chicken", "Leafs", "Grass", "Hay", "Fruits", "Insects")

    feedAnimals(listOfAnimals, listOfFood)
}


abstract class Animal(val name: String, val height: Int, val weight: Int){
    abstract var satiety: Int
    abstract val preferredFood: Array<String>
    fun eat(foodList: Array<String>){
        for (food in foodList){
            if (food in preferredFood){
                satiety++
                println("$name поел! Теперь его сытость равна $satiety\n")
                break
            }
        }
    }
}
abstract class Predator(name: String, height: Int, weight: Int):
               Animal(name, height, weight){
    override val preferredFood = arrayOf("Meat", "Milk", "Fish", "Bones", "Chicken")
    override var satiety: Int = 0
}
abstract class Herbivore(name: String, height: Int, weight: Int):
               Animal(name, height, weight){
    override val preferredFood = arrayOf("Leafs", "Grass", "Hay", "Fruits", "Insects")
    override var satiety: Int = 0
}
class Lion(name: String,height: Int, weight: Int):
      Predator(name, height, weight){
      }
class Tiger(name: String,height: Int, weight: Int):
      Predator(name, height, weight){
      }
class Hippo(name: String,height: Int, weight: Int):
      Herbivore(name, height, weight){
      }
class Wolf(name: String,height: Int, weight: Int):
      Predator(name, height, weight){
      }
class Giraffe(name: String,height: Int, weight: Int):
      Herbivore(name, height, weight){
      }
class Elephant(name: String,height: Int, weight: Int):
      Herbivore(name, height, weight){
      }
class Chimpanzee(name: String,height: Int, weight: Int):
      Herbivore(name, height, weight){
      }
class Gorilla(name: String,height: Int, weight: Int):
      Herbivore(name, height, weight){
      }
fun feedAnimals(animalsList: Array<Animal>, foodList: Array<String>){
    for (animal in animalsList){
        animal.eat(foodList)
    }
}
