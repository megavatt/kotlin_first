package homework5
fun main(){
    val alex = Lion("Alex", 90, 190)
    val vitaly = Tiger("Vitaly", 80, 310)
    val gloria = Hippo("Gloria", 130, 1300)
    val kowalski = Wolf("Kowalski", 80, 80)
    val melman = Giraffe("Melman", 420, 1200)
    val sonya = Elephant("Sonya", 280, 3000)
    val mason = Chimpanzee("Mason", 120, 50)
    val julien = Gorilla("Julien", 160, 180)

    println("Льва зовут ${alex.name} его рост ${alex.height} см. и вес ${alex.weight} кг.")
    alex.eat("Meat")
    println("Тигра зовут ${vitaly.name} его рост ${vitaly.height} см. и вес ${vitaly.weight} кг.")
    vitaly.eat("Chicken")
    println("Бегемота зовут ${gloria.name} его рост ${gloria.height} см. и вес ${gloria.weight} кг.")
    gloria.eat("Leafs")
    println("Волка зовут ${kowalski.name} его рост ${kowalski.height} см. и вес ${kowalski.weight} кг.")
    kowalski.eat("Fish")
    println("Жирафа зовут ${melman.name} его рост ${melman.height} см. и вес ${melman.weight} кг.")
    melman.eat("Grass")
    println("Слона зовут ${sonya.name} его рост ${sonya.height} см. и вес ${sonya.weight} кг.")
    sonya.eat("Hay")
    println("Шимпанзе зовут ${mason.name} его рост ${mason.height} см. и вес ${mason.weight} кг.")
    mason.eat("Insects")
    println("Гориллу зовут ${julien.name} его рост ${julien.height} см. и вес ${julien.weight} кг.")
    julien.eat("Fruits")
}

class Lion (val name: String, val height: Int, val weight: Int){
    var lionPreferredFood = arrayOf("Meat", "Milk", "Fish")
    var satiety: Int = 0

    fun eat(food: String){
        if (food in lionPreferredFood) {
            satiety++
            println("${this.name} поел! Теперь его сытость равна ${this.satiety}\n")
        }
        else{
            println("${this.name} такое не ест.\n")
        }
    }
}

class Tiger (val name: String, val height: Int, val weight: Int){
    var tigerPreferredFood = arrayOf("Meat", "Chicken", "Fish")
    var satiety: Int = 0

    fun eat(food: String){
        if (food in tigerPreferredFood) {
            satiety++
            println("${this.name} поел! Теперь его сытость равна ${this.satiety}\n")
        }
        else{
            println("${this.name} такое не ест.\n")
        }
    }
}

class Hippo (val name: String, val height: Int, val weight: Int){
    var hippoPreferredFood = arrayOf("Leafs", "Grass", "Hay")
    var satiety: Int = 0

    fun eat(food: String){
        if (food in hippoPreferredFood) {
            satiety++
            println("${this.name} поел! Теперь его сытость равна ${this.satiety}\n")
        }
        else{
            println("${this.name} такое не ест.\n")
        }
    }
}

class Wolf (val name: String, val height: Int, val weight: Int){
    var wolfPreferredFood = arrayOf("Meat", "Fish", "Bones")
    var satiety: Int = 0

    fun eat(food: String){
        if (food in wolfPreferredFood) {
            satiety++
            println("${this.name} поел! Теперь его сытость равна ${this.satiety}\n")
        }
        else{
            println("${this.name} такое не ест.\n")
        }
    }
}

class Giraffe (val name: String, val height: Int, val weight: Int){
    var giraffePreferredFood = arrayOf("Leafs", "Grass", "Fruits")
    var satiety: Int = 0

    fun eat(food: String){
        if (food in giraffePreferredFood) {
            satiety++
            println("${this.name} поел! Теперь его сытость равна ${this.satiety}\n")
        }
        else{
            println("${this.name} такое не ест.\n")
        }
    }
}

class Elephant (val name: String, val height: Int, val weight: Int){
    var elephantPreferredFood = arrayOf("Grass", "Hay", "Fruits")
    var satiety: Int = 0

    fun eat(food: String){
        if (food in elephantPreferredFood) {
            satiety++
            println("${this.name} поел! Теперь его сытость равна ${this.satiety}\n")
        }
        else{
            println("${this.name} такое не ест.\n")
        }
    }
}

class Chimpanzee (val name: String, val height: Int, val weight: Int){
    var chimpanzeePreferredFood = arrayOf("Fruits", "Leafs", "Insects")
    var satiety: Int = 0

    fun eat(food: String){
        if (food in chimpanzeePreferredFood) {
            satiety++
            println("${this.name} поел! Теперь его сытость равна ${this.satiety}\n")
        }
        else{
            println("${this.name} такое не ест.\n")
        }
    }
}

class Gorilla (val name: String, val height: Int, val weight: Int){
    var gorillaPreferredFood = arrayOf("Grass", "Leafs", "Fruits")
    var satiety: Int = 0

    fun eat(food: String){
        if (food in gorillaPreferredFood) {
            satiety++
            println("${this.name} поел! Теперь его сытость равна ${this.satiety}\n")
        }
        else{
            println("${this.name} такое не ест.\n")
        }
    }
}