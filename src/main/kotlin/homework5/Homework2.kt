package homework5

fun main() {
    print("Введи число, которое нужно перевернуть: ")
    val digit = readLine()!!.toInt()
    print("Твое перевернутое число: ${reverseDigits(digit)}")
}

fun reverseDigits(number: Int): Int{
    return number.toString().reversed().toInt()
}
