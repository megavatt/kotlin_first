package homework4

fun main() {
    val marks = arrayOf(3, 4, 5, 2, 3, 5, 5, 2, 4, 5, 2, 4, 5, 3, 4, 3, 3, 4, 4, 5)
    var studentGradeA = 0
    var studentGradeB = 0
    var studentGradeC = 0
    var studentGradeD = 0

    for (score in marks){
        when(score){
            5 -> studentGradeA++
            4 -> studentGradeB++
            3 -> studentGradeC++
            else -> studentGradeD++
        }
    }

    fun count(numberOfStudents: Int): String {
        val sum: Float = ((numberOfStudents / marks.size.toFloat()) * 100)
        return "%.1f".format(sum)
    }

    println("Отличников - ${count(studentGradeA)}%")
    println("Хорошистов - ${count(studentGradeB)}%")
    println("Троечников - ${count(studentGradeC)}%")
    println("Двоечников - ${count(studentGradeD)}%")
}