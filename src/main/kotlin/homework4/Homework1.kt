package homework4

fun main() {
    val myArray = arrayOf(1, 25, 10, 6, 22, 17, 8, 14, 23)
    var joeBag: Int = 0
    var teamBag: Int = 0

    for (coin in myArray){
        if (coin % 2 == 0){
            joeBag++
        } else {
            teamBag++
        }
    }
    println("Весь клад из ${myArray.size} монет поделен! У Джо $joeBag монет, а у команды $teamBag монет!")
}
