package homework3

fun main() {
    val lemonade : Int = 18500
    val pinaColada: Short = 200
    val whiskey: Byte = 50
    val fresh: Long = 3000000000L
    val cola: Float = 0.5F
    val ale: Double = 0.666666667
    val authorsDrink: String = "Что-то авторское!"

    println("Заказ - \'$lemonade мл лимонада\' готов!")
    println("Заказ - \'$pinaColada мл пина-колады\' готов!")
    println("Заказ - \'$whiskey мл виски\' готов!")
    println("Заказ - \'$fresh капель фреша\' готов!")
    println("Заказ - \'$cola литра колы\' готов!")
    println("Заказ - \'$ale литра эля\' готов!")
    println("Заказ - \'$authorsDrink\' готов!")
}