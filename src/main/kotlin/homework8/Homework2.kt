package homework8

val userCart = mutableMapOf<String, Int>("potato" to 2, "cereal" to 2, "milk" to 1, "sugar" to 3, "onion" to 1, "tomato" to 2, "cucumber" to 2, "bread" to 3)
val discountSet = setOf("milk", "bread", "sugar")
val discountValue = 0.20
val vegetableSet = setOf("potato", "tomato", "onion", "cucumber")
val prices = mutableMapOf<String, Double>(
    "potato" to 33.0,
    "sugar" to 67.5,
    "milk" to 58.7,
    "cereal" to 78.4,
    "onion" to 23.76,
    "tomato" to 88.0,
    "cucumber" to 68.4,
    "bread" to 22.0
)

fun main() {
    print("Total vegetables in cart: ")
    print(countVegetables(userCart, vegetableSet))

    print("\nTotal cart price is: ")
    print(calculateCartPrice(userCart, discountSet, discountValue, prices))

}

fun countVegetables(cart: Map<String, Int>, vegetables: Set<String>): Int {
    var totalVegetables: Int = 0
    for ((key, value) in cart) {
        if (key in vegetables) totalVegetables += value
    }
    return totalVegetables
}

fun calculateCartPrice(cart: Map<String, Int>,
              discountProduct: Set<String>,
              discountRate: Double,
              productPrices: Map<String, Double>): Double {
    var cartPrice: Double = 0.0
    val discount: Double = 1 - discountRate
    for ((key, value) in cart) {
        if (key in discountProduct) cartPrice += (value * (productPrices[key]!! * discount))
        else cartPrice += (value * productPrices[key]!!)
    }
    return cartPrice
}