package homework8

fun main() {
    val sourceList = mutableListOf(1, 2, 3, 1, 2, 3, 1, 1, 5, 6, 8, 1, 8, 7, 4, 9)

    print("List of unique values is: ")
    print(makeListUnique(sourceList))
}

fun makeListUnique(inputList: List<Int>): List<Int> {
    return inputList.toSet().toList()
}
