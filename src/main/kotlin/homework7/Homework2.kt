package homework7

fun main() {
    try {
        exceptionTest(readLine()!!.toInt())
    } catch (e: CustomException5) {
        println("Поймано исключение CustomException5")
    } catch (e: CustomException2) {
        println("Поймано исключение CustomException2")
    } catch (e: CustomException4) {
        println("Поймано исключение CustomException4")
    } catch (e: CustomException3) {
        println("Поймано исключение CustomException3")
    } catch (e: CustomException1) {
        println("Поймано исключение CustomException1")
    } catch (e: Exception) {
        println("Поймано исключение Exception")
    }
}

open class CustomException1(message: String) : Exception(message)
open class CustomException2(message: String) : CustomException1(message)
open class CustomException3(message: String) : Exception(message)
open class CustomException4(message: String) : Exception(message)
open class CustomException5(message: String) : CustomException4(message)

fun exceptionTest(n: Int) {
    when(n) {
        1 -> throw CustomException1("CustomException1")
        2 -> throw CustomException2("CustomException2")
        3 -> throw CustomException3("CustomException3")
        4 -> throw CustomException4("CustomException4")
        5 -> throw CustomException5("CustomException5")
        else -> throw Exception("Exception")
    }
}