package homework7

import java.lang.Exception

fun main() {
    print("Введите логин: ")
    val inputLogin: String = readLine()!!.toString()
    print("Введите пароль: ")
    val inputPassword: String = readLine()!!.toString()
    print("Введите пароль еще раз: ")
    val confirmPassword: String = readLine()!!.toString()

    createUser(inputLogin, inputPassword, confirmPassword)
}
fun createUser(login: String, password: String, passwordConfirmation: String): User{
    if(login.length > 20){
        throw WrongLoginException()
    }
    else if(password.length < 10 || password != passwordConfirmation){
        throw WrongPasswordException()
    }

    println("\nUser successfully created.\nUsername: $login\nPassword: $password")
    return User(login, password)
}

class WrongLoginException: Exception("Maximum login length is 20 symbols")
class WrongPasswordException: Exception("Passwords should contain at least 10 symbols and must match")
class User(val login: String, val password: String)